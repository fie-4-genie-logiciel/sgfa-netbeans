/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgfa;

import java.util.LinkedList;

// On utilise une liste pour représenter une file d'attente
class Queue extends LinkedList<ServiceRequest> {};

/**
 *
 * @author rbastide
 */
public class RequestQueue {
        public static final int FIRST_PRO_NUMBER = 1000;
        public static final int FIRST_NONPRO_NUMBER = 0;
	// Le numéro de requête, incrémenté 
	private int proRequestNumber = FIRST_PRO_NUMBER;
	private int nonProRequestNumber = FIRST_NONPRO_NUMBER;

	// Les deux files d'attente
	private final Queue pro = new Queue();
	private final Queue nonPro = new Queue();
	
	public ServiceRequest newRequest(RequestType type) {
		ServiceRequest result;
		if (type == RequestType.GUICHET_PRO) {
			result = new ServiceRequest(++proRequestNumber, type);
			pro.add(result);
		} else {
			result = new ServiceRequest(++nonProRequestNumber, type);
			nonPro.add(result);
		}
		return result;
	}
	
	public ServiceRequest requestForPro() {
		return getFirstInQueue(pro);
	}

	public ServiceRequest requestForNonPro() {
		return getFirstInQueue(nonPro);
	}

	private ServiceRequest getFirstInQueue(Queue queue) {
		if (queue.size() > 0)
			return queue.remove(0);
		else
			return null;
		
	}
}
