/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgfa;

/**
 *
 * @author rbastide
 */
public class Employee {

	public Employee(int number, String name, boolean pro) {
		this.number = number;
		this.name = name;
		this.habilitePro = pro;
	}
	
	private int number;

	/**
	 * Get the value of number
	 *
	 * @return the value of number
	 */
	public int getNumber() {
		return number;
	}

	private String name;

	/**
	 * Get the value of name
	 *
	 * @return the value of name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the value of name
	 *
	 * @param name new value of name
	 */
	public void setName(String name) {
		this.name = name;
	}
	private boolean habilitePro;

	/**
	 * Get the value of habilitePro
	 *
	 * @return the value of habilitePro
	 */
	public boolean isHabilitePro() {
		return habilitePro;
	}

}
