/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgfa;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author rbastide
 */
public class EmployeeDAO {
	private Map<Integer, Employee> database = new HashMap<>();
	
	public EmployeeDAO() {
		database.put(1, new Employee(1, "Georges Untel", true));
		database.put(2, new Employee(2, "Paul Truc", false));
		database.put(3, new Employee(3, "Elise Machin", true));
		database.put(4, new Employee(4, "Jacques Chose", false));
		database.put(5, new Employee(5, "Pierre Bouzin", false));
		database.put(6, new Employee(6, "Mathieu Quelqun", true));
	}
	
	public Employee findEmployee(int number) {
		return database.get(number);
	}
}
