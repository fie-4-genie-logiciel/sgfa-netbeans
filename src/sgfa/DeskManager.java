/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgfa;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author bastide
 */

class AtDesk {
	int deskNumber;
	boolean available = false;
	AtDesk(int deskNumber) {
		this.deskNumber = deskNumber;
	}
}

public class DeskManager {
	private static final Logger logger = Logger.getLogger(DeskManager.class.getCanonicalName());
	private final Map<Employee, AtDesk> employeesAtDesk = new HashMap<>();

	public DeskManager() {
	}
	
	/**
	 * Appelé par Authentification quand un employé se connecte à son guichet
	 * @param employeeNumber le numéro de l'employé
	 * @param deskNumber le numéro du guichet
	 */
	public void employeeConnected(Employee employee, int deskNumber) {
		logger.log(Level.INFO, "L''employé {0} vient de se connecter au guichet {1}", new Object[]{employee.getName(), deskNumber});
		employeesAtDesk.put(employee, new AtDesk(deskNumber));
	}
	
	/**
	 * Appelé par Authentification quand un employé se déconnecte de son guichet
	 * @param employeeNumber le numéro de l'employé
	 */
	public void employeeDisconnected(Employee employee) {
		logger.log(Level.INFO, "L''employé {0} vient de se déconnecter de son guichet", employee.getName());
		employeesAtDesk.remove(employee);
	}
	
	public void employeeAvailable(Employee employee) {
		setAvailable(employee, true);
	}

	public void employeeBusy(Employee employee) {
		setAvailable(employee, false);
	}
	
	private void setAvailable(Employee employee, boolean available) {
		AtDesk atDesk = employeesAtDesk.get(employee);
		if (atDesk != null)
			atDesk.available = available;
		else
			logger.log(Level.SEVERE, "L''employé {0} n'est pas connecté", employee.getName());		
	}
	
	int deskForPro() throws Exception {
		for (Employee e : employeesAtDesk.keySet()) { // Pour chaque employé connecté
			if (e.isHabilitePro()) { // Si il est habilité pro
				AtDesk ad = employeesAtDesk.get(e);
				if (ad.available) // Si il est disponible
					return ad.deskNumber; // On renvoie son numéro de guichet
			}
		}
		// On n'a trouvé aucun habilité pro disponible
		throw new Exception("Aucun habilité pro disponible");
	}

	int deskForNonPro() throws Exception {
		for (Employee e : employeesAtDesk.keySet()) { // Pour chaque employé connecté
				AtDesk ad = employeesAtDesk.get(e);
				if (ad.available) // Si il est disponible
					return ad.deskNumber; // On renvoie son numéro de guichet
		}
		// On n'a trouvé aucun habilité pro disponible
		throw new Exception("Aucun employé disponible");
	}
	
	Employee findEmployeeAtDesk(int deskNumber) {
		Employee result = null;
		for (Employee e : employeesAtDesk.keySet()) {
			AtDesk ad = employeesAtDesk.get(e);
			if (ad.deskNumber == deskNumber) {
				result = e;
				break;
			}
		}
		return result;
	}
}
