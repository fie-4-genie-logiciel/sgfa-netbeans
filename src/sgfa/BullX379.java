/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgfa;

/**
 *
 * @author rbastide
 */
public class BullX379 extends javax.swing.JFrame {

	final SGFA systeme;
	/**
	 * Creates new form BullX379
	 */
	public BullX379(SGFA systeme) {
		this.systeme = systeme;
		initComponents();
		setVisible(true);
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {		
		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new BullX379(null);
			}
		});
	}	
	
	public void printTicket(int ticketNumber, RequestType type) {
		new Ticket(ticketNumber, type);
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jLabel1 = new javax.swing.JLabel();
                jPanel1 = new javax.swing.JPanel();
                bouronRetrait = new javax.swing.JButton();
                boutonPro = new javax.swing.JButton();
                boutonToutesOperations = new javax.swing.JButton();

                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                setTitle("BullX379");

                jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
                jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabel1.setText("<html><center>Choisissez votre opération,<br> conservez le ticket<br>attendez l'appel de votre numéro sur les écrans d'affichage</center></html>");
                jLabel1.setPreferredSize(new java.awt.Dimension(400, 80));
                getContentPane().add(jLabel1, java.awt.BorderLayout.PAGE_START);

                bouronRetrait.setText("Retrait d'instances (colis, recommandés)");
                bouronRetrait.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                bouronRetraitActionPerformed(evt);
                        }
                });
                jPanel1.add(bouronRetrait);

                boutonPro.setText("Opérations réservées au professionnels");
                boutonPro.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                boutonProActionPerformed(evt);
                        }
                });
                jPanel1.add(boutonPro);

                boutonToutesOperations.setText("Toutes opérations");
                boutonToutesOperations.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                boutonToutesOperationsActionPerformed(evt);
                        }
                });
                jPanel1.add(boutonToutesOperations);

                getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

                pack();
        }// </editor-fold>//GEN-END:initComponents

        private void bouronRetraitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bouronRetraitActionPerformed
                systeme.newRequest(RequestType.RETRAIT_INSTANCES);
        }//GEN-LAST:event_bouronRetraitActionPerformed

        private void boutonProActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boutonProActionPerformed
                systeme.newRequest(RequestType.GUICHET_PRO);
        }//GEN-LAST:event_boutonProActionPerformed

        private void boutonToutesOperationsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boutonToutesOperationsActionPerformed
                systeme.newRequest(RequestType.TOUTES_OPERATIONS);
        }//GEN-LAST:event_boutonToutesOperationsActionPerformed

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton bouronRetrait;
        private javax.swing.JButton boutonPro;
        private javax.swing.JButton boutonToutesOperations;
        private javax.swing.JLabel jLabel1;
        private javax.swing.JPanel jPanel1;
        // End of variables declaration//GEN-END:variables
}
