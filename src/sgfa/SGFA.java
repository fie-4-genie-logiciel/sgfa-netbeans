/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgfa;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rbastide
 */
public class SGFA {
	private static final Logger logger = Logger.getLogger(SGFA.class.getCanonicalName());
	
	private final BullX379 borne;
	private final RequestQueue requestQueue;
	private final Dispatcher dispatcher;
	private final DeskManager deskManager;
	private final DisplayScreen screen;
	private final EmployeeDAO employeeDatabase;
	private final Map<Employee, EmployeeUI> employeeUIs = new HashMap<>();

	public SGFA() {
		borne = new BullX379(this);
		requestQueue = new RequestQueue();
		dispatcher = new Dispatcher();
		deskManager = new DeskManager();
		screen = new DisplayScreen();
		employeeDatabase = new EmployeeDAO();
	}


	public void newRequest(RequestType type) {
		ServiceRequest nouvelle = requestQueue.newRequest(type);
		logger.log(Level.INFO, "Nouvelle requête n° {0}, type {1}", new Object[]{nouvelle.getNumero(), nouvelle.getType()});
		borne.printTicket(nouvelle.getNumero(), nouvelle.getType());
		dispatchRequest();
	}
	
	void employeeAvailable(Employee employee) {
		logger.log(Level.INFO, "L''employé {0}, habilité : {1} est disponible", new Object[]{employee.getName(), employee.isHabilitePro()});
		deskManager.employeeAvailable(employee);
		dispatchRequest();
	}

	void employeeBusy(Employee employee) {
		logger.log(Level.INFO, "L''employé {0}, habilité : {1} n''est plus disponible", new Object[]{employee.getName(), employee.isHabilitePro()});
		deskManager.employeeBusy(employee);		
	}

	void requestCompleted(ServiceRequest request){
		logger.log(Level.INFO, "Requête n° {0}, type {1} terminée", new Object[]{request.getNumero(), request.getType()});		
		request.termineService();
		screen.clearDesk(request.getGuichet(), request.getNumero());
			// TODO : sauvegarder la requête
	}
	
	void employeeConnected(int employeeNumber, int deskNumber) {
		Employee employee = employeeDatabase.findEmployee(employeeNumber);
		if (employee != null) {
			deskManager.employeeConnected(employee, deskNumber);
			employeeUIs.put(employee, new EmployeeUI(this, employee, deskNumber));
		} else
			logger.log(Level.SEVERE, "L''employé N°{0} est inconnu dans la base", employeeNumber);			
	}

	void employeeDisconnected(int employeeNumber) {
		Employee employee = employeeDatabase.findEmployee(employeeNumber);
		if (employee != null) {
			deskManager.employeeDisconnected(employee);
			employeeUIs.get(employee).dispose();
		} else
			logger.log(Level.SEVERE, "L''employé N°{0} est inconnu dans la base", employeeNumber);			
	}
	
	private void dispatchRequest() {
		ServiceRequest choisie = dispatcher.tryDispatch(requestQueue, deskManager);
		if (choisie != null) {
			logger.log(Level.INFO, "La requête n° {0}, type {1} va être servie au guichet {2}", new Object[]{choisie.getNumero(), choisie.getType(), choisie.getGuichet()});			
			screen.showDesk(choisie.getNumero(), choisie.getGuichet());
			EmployeeUI ui = employeeUIs.get(choisie.getEmployee());
			ui.showNewrequest(choisie);
			this.employeeBusy(choisie.getEmployee());
		}
	}
	
	public static void main(String[] args) {
                System.out.printf("Logs dans %s%n", System.getProperty("user.home"));
		SGFA systeme = new SGFA();
		// Simule la connection au service d'authentification
		new Authentification(systeme);
	}
}
