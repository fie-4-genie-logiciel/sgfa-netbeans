/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgfa;

import java.util.Calendar;

/**
 *
 * @author rbastide
 */
public class ServiceRequest {
	private final int numero;
	private final RequestType type;
	private final Calendar debutDemande = Calendar.getInstance();
	// Renseignés au début du service de cette ServiceRequest
	private Calendar debutService = null;
	private Integer guichet = null;
	private Employee employee = null;
	// Renseignés à la fin du service de cette ServiceRequest
	private Calendar finService = null;
	

	public ServiceRequest(int numero, RequestType type) {
		this.numero = numero;
		this.type = type;
	}
	
	/**
	 * Enregistre le début du service pour cette ServiceRequest
	 * @param guichet le guichet où cette ServiceRequest est servie
	 * @param employee l'employé qui sert cette ServiceRequest
	 */
	public void demarreService(int guichet, Employee employee) {
		this.setGuichet(guichet);
		this.setEmployee(employee);
		this.setDebutService(Calendar.getInstance());
	}

	/**
	 * Enreigistre la fin de service de cette ServiceRequest
	 */
	public void termineService() {
		this.finService = Calendar.getInstance();
	}	

	/**
	 * Get the value of numero
	 *
	 * @return the value of numero
	 */
	public int getNumero() {
		return numero;
	}
	
	/**
	 * Get the value of debutDemande
	 *
	 * @return the value of debutDemande
	 */
	public Calendar getDebutDemande() {
		return debutDemande;
	}


	/**
	 * Get the value of debutService
	 *
	 * @return the value of debutService
	 */
	public Calendar getDebutService() {
		return debutService;
	}

	/**
	 * Set the value of debutService
	 *
	 * @param debutService new value of debutService
	 */
	protected void setDebutService(Calendar debutService) {
		this.debutService = debutService;
	}

	/**
	 * Get the value of finService
	 *
	 * @return the value of finService
	 */
	public Calendar getFinService() {
		return finService;
	}

	
	public String duration() {
		if (finService != null) {
			return ((finService.getTimeInMillis() - debutService.getTimeInMillis()) / 1000) + "s"; 
		} else
			return "en cours";
	}
	

	/**
	 * Get the value of guichet
	 *
	 * @return the value of guichet
	 */
	public Integer getGuichet() {
		return guichet;
	}

	/**
	 * Set the value of guichet
	 *
	 * @param guichet new value of guichet
	 */
	protected void setGuichet(Integer guichet) {
		this.guichet = guichet;
	}


	/**
	 * Get the value of type
	 *
	 * @return the value of type
	 */
	public RequestType getType() {
		return type;
	}

	/**
	 * Get the value of employee
	 *
	 * @return the value of employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Set the value of employee
	 *
	 * @param employee new value of employee
	 */
	protected void setEmployee(Employee employee) {
		this.employee = employee;
	}

	
}
