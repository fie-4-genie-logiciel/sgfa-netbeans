/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgfa;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rbastide
 */
public class Dispatcher {
	
	ServiceRequest tryDispatch(RequestQueue requestQueue, DeskManager deskManager) {
		try {
			// On essaie une requête pro
			Integer desk = deskManager.deskForPro();
			ServiceRequest request = requestQueue.requestForPro();
			if (request  !=null) {
				request.demarreService(desk, deskManager.findEmployeeAtDesk(desk));
				return request;
			} 
		} catch (Exception ex) {
				Logger.getLogger(Dispatcher.class.getName()).log(Level.INFO, ex.getMessage());
		}

		try {
			// On essaie une requête normale
			Integer desk = deskManager.deskForNonPro();
			ServiceRequest request = requestQueue.requestForNonPro();
			if (request  !=null) {
				request.demarreService(desk, deskManager.findEmployeeAtDesk(desk));
				return request;
			} 
		} catch (Exception ex) {
				Logger.getLogger(Dispatcher.class.getName()).log(Level.INFO, ex.getMessage());
		}
		return null;
	}
	
}
