/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgfa;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rbastide
 */
public class RequestQueueTest {
    // On déclare l'objet à tester
    RequestQueue instance;
    
    public RequestQueueTest() {
    }
    
    @Before
    public void setUp() {
        //On initialise l'objet à tester (réinitialisé avant chaque test)
         instance = new RequestQueue();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Teste la numérotation des requêtes Pro
     */
    @Test
    public void testNewProRequestNumber() {
        RequestType type = RequestType.GUICHET_PRO;
        ServiceRequest result = instance.newRequest(type);
        assertTrue(result.getNumero() >= RequestQueue.FIRST_PRO_NUMBER);
        type = RequestType.RETRAIT_INSTANCES;
        result = instance.newRequest(type);
        assertTrue(result.getNumero() >= RequestQueue.FIRST_NONPRO_NUMBER);
    }

    /**
     * Teste la méthode RequestForPro();
     */
    @Test
    public void testRequestForPro() {
        ServiceRequest result = instance.requestForPro();
        // sur une RequestQueue vide, requestForPro() renvoie null
        assertNull(result);
        // On crée une requête pro
        instance.newRequest(RequestType.GUICHET_PRO);
        // RequestForPro doit maintenant me renvoyer un résultat
        result = instance.requestForPro();
        assertNotNull(result);
    }

}